const { Router } = require('express')
const ClienteController = require('../controller/ClienteController')
const app = Router()


// Rotas de Clintes.
app.post('/cliente', ClienteController.post);
app.get('/cliente', ClienteController.list);
app.delete('/cliente/delete/:id', ClienteController.delete);
app.get('/cliente/:id', ClienteController.listOne);
app.put('/cliente/edit/:id', ClienteController.update);

module.exports = app;
